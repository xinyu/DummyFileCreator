#include "GenDummyThread.h"

GenDummyThread::GenDummyThread(int fileSize, int fileCount, QString destPath){
    this->fileSize = fileSize;
    this->fileCount = fileCount;
    this->destPath = destPath;

    srand(time(NULL));
    buff = (char*)calloc(SIZE_1M, sizeof(char));
    buffRandomize(SIZE_1M);
}

GenDummyThread::~GenDummyThread()
{
    free(buff);
}

void GenDummyThread::run()
{
    FILE *fp;
    int i = 0, k = 0 , writeSize = 0;
    char fileName[128];

    active = true;

    //sprintf(fileName, "mkdir %s/Dummy/", destPath.toAscii().data());
    //system(fileName);


    for(i = 0; i < fileCount; i++){
        sprintf(fileName, "%s/Dummy/%d.dat", destPath.toLatin1().data(), i);

        fp = fopen(fileName, "w+");

        if(fileSize == RAND_FILE_SIZE){
            writeSize = rand() % (SIZE_1M * 32) + 1;

        }else{
            writeSize = fileSize;
        }

        for(k = 0; k < writeSize / SIZE_1M; k++){
            fwrite(buff, SIZE_1M, 1, fp);
        }
        fwrite(buff, 1, writeSize % SIZE_1M, fp);

        fclose(fp);

        emit updateProgressBar((i + 1) * 100 / fileCount);
    }

    /*
    while(active && (i++ < 100)){
        emit updateProgressBar(i);
        msleep(20);
    }
    */

    emit threadFinished();
}

void GenDummyThread::buffRandomize(int size)
{
    int i;
    for(i = 0; i < size; i++){
        buff[i] = rand() % 256;
    }
}

void GenDummyThread::syncParams(int fileCount, int fileSize, QString destPath)
{
    this->fileSize = fileSize;
    this->fileCount = fileCount;
    this->destPath = destPath;
}

void GenDummyThread::stop()
{
    active  = false;
}
