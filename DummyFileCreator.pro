#-------------------------------------------------
#
# Project created by QtCreator 2015-05-16T09:34:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DummyFileCreator
TEMPLATE = app


SOURCES += main.cpp\
        dummyfilecreator.cpp \
    GenDummyThread.cpp

HEADERS  += dummyfilecreator.h \
    GenDummyThread.h

FORMS    += dummyfilecreator.ui
