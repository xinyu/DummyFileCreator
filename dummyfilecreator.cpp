#include "dummyfilecreator.h"
#include "ui_dummyfilecreator.h"
#include "GenDummyThread.h"

DummyFileCreator::DummyFileCreator(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DummyFileCreator)
{
    ui->setupUi(this);
    ui->sb_size->setDisabled(true);
    destination = QDir().homePath();
    ui->le_dest->setText(destination + "/Dummy/");
    fileSize = RAND_FILE_SIZE;
    fileCount = 1;
    active = 0;

    dummyThread = new GenDummyThread(fileSize, fileCount, destination);
    QObject::connect(dummyThread, SIGNAL(updateProgressBar(int)), this, SLOT(onUpdateProgressBar(int)));
    QObject::connect(dummyThread, SIGNAL(threadFinished()), this, SLOT(onThreadFinished()));
    GetFreeSpace();
}

DummyFileCreator::~DummyFileCreator()
{
    delete ui;
    delete dummyThread;
}

void DummyFileCreator::GetFreeSpace()
{
    struct stat stst;
    struct statfs stfs;

    stat(destination.toLatin1().data(), &stst);
    statfs(destination.toLatin1().data(), &stfs);
    freeSpace = stfs.f_bavail * (stst.st_blksize / 1024) / 1024 / 1024;
    totalSpace = stfs.f_blocks * (stst.st_blksize / 1024) / 1024 / 1024;

    ui->le_avail->setText(QString().setNum(freeSpace) + " GB");
    ui->le_total->setText(QString().setNum(totalSpace) + " GB");

    dummyThread->syncParams(fileCount, fileSize, destination);
}

void DummyFileCreator::onBrowseClicked()
{
    destination = QFileDialog().getExistingDirectory(this, tr("Select Path"), QDir().homePath());
    ui->le_dest->setText(destination + "/Dummy/");
    GetFreeSpace();
}

void DummyFileCreator::onRandomSizeStateChanged(int state)
{
    if(state){
        ui->sb_size->setEnabled(false);
        ui->cb_maxSize->setCheckState(Qt::Unchecked);
        fileSize = RAND_FILE_SIZE;
    }else{
        if(!(ui->cb_maxSize->checkState() || ui->cb_randSize->checkState())){
            ui->sb_size->setEnabled(true);
        }
        fileSize = ui->sb_size->value() * SIZE_1M;
    }
}

void DummyFileCreator::onMaxSizeStateChanged(int state)
{
    if(state){
        ui->sb_size->setEnabled(false);
        ui->cb_randSize->setCheckState(Qt::Unchecked);
        fileSize = MAX_FILE_SIZE;;
    }else{
        if(!(ui->cb_maxSize->checkState() || ui->cb_randSize->checkState())){
            ui->sb_size->setEnabled(true);
        }
        fileSize = ui->sb_size->value() * SIZE_1M;
    }
}

void DummyFileCreator::onSizeValueChanged(int value)
{
    fileSize = value * SIZE_1M;
}

void DummyFileCreator::onGenerateClicked()
{
    if(active){
        active = false;
        dummyThread->stop();
        ui->pb_generate->setText("Generate");
    }else{
        ui->pb_generate->setText("Abort");
        active = true;
        dummyThread->syncParams(fileCount, fileSize, destination);
        dummyThread->start();
    }
}

void DummyFileCreator::onUpdateProgressBar(int value)
{
    ui->progressBar->setValue(value);
}

void DummyFileCreator::onThreadFinished()
{
    active = false;
    ui->pb_generate->setText("Generate");
}

void DummyFileCreator::onClearAllClicked()
{

}

void DummyFileCreator::onFileCountChanged(int value)
{
    fileCount = value;
}

void DummyFileCreator::onMaxFileCountStateChanged(int state)
{
    if(state){
        fileCount = MAX_FILE_COUNT;
        ui->sb_fileCount->setDisabled(true);
    }else{
        ui->sb_fileCount->setEnabled(true);
        fileCount = ui->sb_fileCount->value();
    }
}
