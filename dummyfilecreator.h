#ifndef DUMMYFILECREATOR_H
#define DUMMYFILECREATOR_H

#include <QWidget>
#include <QFileDialog>
#include <sys/vfs.h>
#include <sys/stat.h>
#include <stdio.h>

#include "GenDummyThread.h"

#define MAX_FILE_COUNT  -1
#define RAND_FILE_SIZE  -1
#define MAX_FILE_SIZE   -2
#define SIZE_1M (1024 * 1024)

namespace Ui {
    class DummyFileCreator;
}

class DummyFileCreator : public QWidget
{
    Q_OBJECT

public:

    explicit DummyFileCreator(QWidget *parent = 0);
    ~DummyFileCreator();
    void GetFreeSpace();

public slots:
    void onBrowseClicked();
    void onRandomSizeStateChanged(int);
    void onMaxSizeStateChanged(int);
    void onSizeValueChanged(int);
    void onGenerateClicked();
    void onClearAllClicked();
    void onMaxFileCountStateChanged(int);
    void onFileCountChanged(int);
    void onUpdateProgressBar(int);
    void onThreadFinished();

signals:
    void updateProgressBar(int value);

private:
    GenDummyThread* dummyThread;

    int freeSpace;
    int totalSpace;
    int fileSize;
    int fileCount;
    QString destination;
    bool active;
    Ui::DummyFileCreator *ui;
};

#endif // DUMMYFILECREATOR_H
