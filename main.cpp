#include "dummyfilecreator.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    DummyFileCreator w;
    w.show();

    return a.exec();
}
