#ifndef GENDUMMYTHREAD_H
#define GENDUMMYTHREAD_H

#include <QThread>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_FILE_COUNT  -1
#define RAND_FILE_SIZE  -1
#define MAX_FILE_SIZE   -2
#define SIZE_1M                         (1024 * 1024)
#define SIZE_1G				(1024 * 1024 * 1024)

class GenDummyThread:public QThread
{
    Q_OBJECT

public:
        GenDummyThread(int fileCount, int fileSize, QString destPath);
        virtual ~GenDummyThread();
        void stop();
        void syncParams(int fileCount, int fileSize, QString destPath);

protected:
        void run();
        void buffRandomize(int size);

signals:
        void updateProgressBar(int);
        void threadFinished();

private:
        int fileSize;
        int fileCount;
        bool active;
        char* buff;
        QString destPath;
};

#endif // GENDUMMYTHREAD_H
